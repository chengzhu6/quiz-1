package com.twuc.webApp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.entity.Game;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


@SpringBootTest
@AutoConfigureMockMvc
class GameControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shold_return_201_when_start_game() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/games"))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.header().exists("Location"))
        ;
    }

    @Test
    void should_404_when_get_game_with_not_exist_id() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/games/4444"))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }


    @Test
    void should_200_when_get_game_with_exist_id() throws Exception {

        ResultActions perform = mockMvc.perform(MockMvcRequestBuilders.post("/api/games"));
        MvcResult mvcResult = perform.andReturn();
        String newGameUri = mvcResult.getResponse().getHeader("Location");
        assert newGameUri != null;
        mockMvc.perform(MockMvcRequestBuilders.get(newGameUri))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void should_return_200_when_call_check_api_with_answer() throws Exception {

        ResultActions perform = mockMvc.perform(MockMvcRequestBuilders.post("/api/games"));
        MvcResult mvcResult = perform.andReturn();
        String newGameUri = mvcResult.getResponse().getHeader("Location");
        assert newGameUri != null;

        mockMvc.perform(MockMvcRequestBuilders.patch(newGameUri)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{ \"answer\": \"{1112}\" }"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void should_return_200_when_call_check_api_with_correct_answer() throws Exception {

        ResultActions perform = mockMvc.perform(MockMvcRequestBuilders.post("/api/games"));
        MvcResult mvcResult = perform.andReturn();
        String newGameUri = mvcResult.getResponse().getHeader("Location");
        assert newGameUri != null;


        ResultActions perform1 = mockMvc.perform(MockMvcRequestBuilders.get(newGameUri));
        String gameJson = perform1.andReturn().getResponse().getContentAsString();
        ObjectMapper objectMapper = new ObjectMapper();
        Game game = objectMapper.readValue(gameJson, Game.class);

        mockMvc.perform(MockMvcRequestBuilders.patch(newGameUri)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(String.format("{ \"answer\": \"{%s}\" }", game.getAnswer())))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void should_return_404_when_call_check_api_with_not_exist_id() throws Exception {

        ResultActions perform = mockMvc.perform(MockMvcRequestBuilders.post("/api/games"));
        MvcResult mvcResult = perform.andReturn();
        String newGameUri = mvcResult.getResponse().getHeader("Location");
        assert newGameUri != null;


        ResultActions perform1 = mockMvc.perform(MockMvcRequestBuilders.get(newGameUri));
        String gameJson = perform1.andReturn().getResponse().getContentAsString();
        ObjectMapper objectMapper = new ObjectMapper();
        Game game = objectMapper.readValue(gameJson, Game.class);

        mockMvc.perform(MockMvcRequestBuilders.patch(newGameUri + "1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(String.format("{ \"answer\": \"{%s}\" }", game.getAnswer())))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    void should_return_400_when_call_check_api_with_illegal_answer() throws Exception {

        ResultActions perform = mockMvc.perform(MockMvcRequestBuilders.post("/api/games"));
        MvcResult mvcResult = perform.andReturn();
        String newGameUri = mvcResult.getResponse().getHeader("Location");
        assert newGameUri != null;

        mockMvc.perform(MockMvcRequestBuilders.patch(newGameUri + "a")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{ \"answer\": \"{123}\" }"))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }
}