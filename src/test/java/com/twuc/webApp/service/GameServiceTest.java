package com.twuc.webApp.service;

import com.twuc.webApp.cache.GameCache;
import com.twuc.webApp.entity.CheckResult;
import com.twuc.webApp.entity.Game;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GameServiceTest {

    @Test
    void should_create_new_game() {

        GameCache gameCache = new GameCache();

        GameService gameService = new GameService(gameCache);
        Game game = gameService.createGame();

        assertNotNull(game.getId());
        assertNotNull(game.getAnswer());
        assertNotNull(game);
        assertEquals(4, game.getAnswer().length());
    }


    @Test
    void should_get_old_game() {

        GameCache gameCache = new GameCache();
        Game exceptGame = new Game(1234L, "1234");
        gameCache.pushGame(exceptGame);
        GameService gameService = new GameService(gameCache);

        Game actualGame = gameService.getGame(1234L);
        assertEquals(exceptGame,actualGame);
    }


    @Test
    void should_get_check_result_false_when_check_incorrect_answer() {
        GameCache gameCache = new GameCache();
        Game exceptGame = new Game(1234L, "1234");
        gameCache.pushGame(exceptGame);
        GameService gameService = new GameService(gameCache);
        CheckResult checkResult = gameService.checkAnswer(1234L, "1345");
        assertNotNull(checkResult);
        assertEquals(false, checkResult.getCorrect());
        assertEquals("1A2B", checkResult.getHint());
    }


    @Test
    void should_get_check_result_true_when_check_correct_answer() {
        GameCache gameCache = new GameCache();
        Game exceptGame = new Game(1234L, "1234");
        gameCache.pushGame(exceptGame);
        GameService gameService = new GameService(gameCache);
        CheckResult checkResult = gameService.checkAnswer(1234L, "1234");
        assertNotNull(checkResult);
        assertEquals(true, checkResult.getCorrect());
    }


}