package com.twuc.webApp.cache;


import com.twuc.webApp.entity.Game;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class GameCache {

    private final Map<Long ,Game> games = new HashMap<>();

    public void pushGame(Game game) {
        games.put(game.getId(), game);
    }

    public Game getGame(Long gameId) {
        return games.get(gameId);
    }
}
