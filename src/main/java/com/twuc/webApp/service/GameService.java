package com.twuc.webApp.service;


import com.twuc.webApp.cache.GameCache;
import com.twuc.webApp.entity.CheckResult;
import com.twuc.webApp.entity.Game;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class GameService {

    private GameCache gameCache;


    @Autowired
    public GameService(GameCache gameCache) {
        this.gameCache = gameCache;
    }

    public GameService() {
    }

    public Game createGame() {
        Random random = new Random();
        Long id = random.nextLong();
        String answer = String.format("%s%s%s%s", random.nextInt(10), random.nextInt(10), random.nextInt(10), random.nextInt(10));
        Game game = new Game(id, answer);
        gameCache.pushGame(game);
        return game;
    }

    public Game getGame(Long gameId) {
        Game game = gameCache.getGame(gameId);
        if (game == null) {
            throw new IllegalArgumentException();
        }
        return game;
    }


    public CheckResult checkAnswer(long gameId, String answer) {
        Game game = gameCache.getGame(gameId);
        if (game == null) {
            throw new IllegalArgumentException();
        }
        return getResult(game.getAnswer(), answer);
    }

    private CheckResult getResult(String exceptAnswer, String actualAnswer) {
        CheckResult result;
        if (exceptAnswer.equals(actualAnswer)) {
            result = new CheckResult("4A0B", true);
        } else {
            String hint = getHint(exceptAnswer, actualAnswer);
            result = new CheckResult(hint, false);
        }
        return result;
    }

    private String getHint(String exceptAnswer, String actualAnswer) {
        int correct = 0;
        int contains = 0;
        for (int startIndex = 0; startIndex < 4; startIndex++) {
            for (int exceptIndex = 0; exceptIndex < 4; exceptIndex++) {
                if (actualAnswer.charAt(startIndex) == exceptAnswer.charAt(exceptIndex)) {
                    if (startIndex != exceptIndex) {
                        contains++;
                    } else {
                        correct++;
                    }
                }
            }
        }
        return String.format("%sA%sB", correct, contains);
    }


}
