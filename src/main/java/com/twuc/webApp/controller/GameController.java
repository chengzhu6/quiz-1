package com.twuc.webApp.controller;


import com.twuc.webApp.entity.Answer;
import com.twuc.webApp.entity.CheckResult;
import com.twuc.webApp.entity.Game;
import com.twuc.webApp.service.GameService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api")
@CrossOrigin
public class GameController {

    private GameService gameService;

    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @PostMapping("/games")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity startGame(){
        Game game = gameService.createGame();
        return ResponseEntity.status(HttpStatus.CREATED.value())
                .header("Location" , String.format("/api/games/%s", game.getId()))
                .header("Access-Control-Expose-Headers", "Location")
                .build();
    }

    @GetMapping("/games/{gameId}")
    public ResponseEntity<Game> getGame(@PathVariable Long gameId){
        Game game = gameService.getGame(gameId);
        return ResponseEntity.status(HttpStatus.OK.value())
                .body(game);
    }

    @PatchMapping("/games/{gameId}")
    public ResponseEntity<CheckResult> checkAnswer(@PathVariable Long gameId, @RequestBody  Answer answer){
        CheckResult checkResult = gameService.checkAnswer(gameId, answer.getAnswer());
        return ResponseEntity.status(HttpStatus.OK.value())
                .body(checkResult);
    }
}
