package com.twuc.webApp.entity;

public class CheckResult {

    private String hint;

    private Boolean correct;

    public CheckResult(String hint, Boolean correct) {
        this.hint = hint;
        this.correct = correct;
    }

    public CheckResult() {
    }

    public String getHint() {
        return hint;
    }

    public Boolean getCorrect() {
        return correct;
    }
}
