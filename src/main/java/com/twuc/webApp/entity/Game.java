package com.twuc.webApp.entity;

public class Game {

    private Long id;

    private String answer;

    public Game() {
    }

    public Game(Long id, String answer) {
        this.id = id;
        this.answer = answer;
    }

    public Long getId() {
        return id;
    }

    public String getAnswer() {
        return answer;
    }
}
